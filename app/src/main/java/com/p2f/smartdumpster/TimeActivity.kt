package com.p2f.smartdumpster

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.util.*
import java.util.function.Consumer

class TimeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_time)
        /////////////////////////////////
        // Add extend button behaviour //
        /////////////////////////////////
        val extendTimerButton: Button = findViewById(R.id.extend_time_button)
        val extendTimeAmountSlider: SeekBar = findViewById(R.id.extend_time_amount)
        extendTimerButton.setOnClickListener {
            val amount = extendTimeAmountSlider.progress
            if (amount > 0) {
                val message = JSONObject()
                message.put("request", "extend_time")
                message.put("amount", amount)
                BluetoothThread.expectEcho = true
                BluetoothThread.sendMessage(message)
                extendTimerButton.isEnabled = false
            }
        }
        ////////////////////////////////
        // Add closing event callback //
        ////////////////////////////////
        GoToMainActivity.initialize(this)
        ExtendTimeDepleted.initialize(extendTimerButton)
        BluetoothThread.onClosingEventReceived.add(GoToMainActivity)
        BluetoothThread.onExtendTimeEventReceived.add(ExtendTime)
        BluetoothThread.onExtendTimeDepletedEventReceived.add(ExtendTimeDepleted)
        ///////////
        // Timer //
        ///////////
        val timeRemainingTextView: TextView = findViewById(R.id.time_remaining_amount)
        timeRemainingTextView.text = "60"
        TimerManager.initialize(timeRemainingTextView, extendTimerButton)
        TimerManager.start(60000)
    }

    object GoToMainActivity : Runnable {

        private lateinit var context: Context

        fun initialize(context: Context) {
            this.context = context
        }

        override fun run() {
            val queue = Volley.newRequestQueue(context.applicationContext)
            val token = Token.value.toString()
            val url = "https://p2f-iot.herokuapp.com/deposits/store/$token"
            val stringRequest = StringRequest(Request.Method.GET, url, {
                val reply = JSONObject(it)
                if (reply.has("message") && reply.has("code")) {
                    val message = reply.getString("message")
                    val code = reply.getInt("code")
                    toastPrint("($code) $message")
                }
            }, {
                Response.ErrorListener {
                    toastPrint(it.localizedMessage)
                }
            })
            queue.add(stringRequest)
            Token.value = null // Reset token singleton
            BluetoothThread.onClosingEventReceived.clear()
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }

        private fun toastPrint(text: String) {
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
        }
    }

    object ExtendTime : Consumer<Int> {

        override fun accept(seconds: Int) {
            TimerManager.extendBy(seconds)
        }
    }

    object ExtendTimeDepleted : Consumer<Int> {

        private lateinit var extendTimerButton: Button

        fun initialize(extendTimerButton: Button) {
            this.extendTimerButton = extendTimerButton
        }

        override fun accept(seconds: Int) {
            TimerManager.extendBy(seconds)
            extendTimerButton.isEnabled = false
        }
    }

    object TimerManager {

        private var timer: BetterTimer? = null
        private lateinit var timeRemainingTextView: TextView
        private lateinit var extendTimerButton: Button

        fun initialize(timeRemainingTextView: TextView, extendTimerButton: Button) {
            this.timeRemainingTextView = timeRemainingTextView
            this.extendTimerButton = extendTimerButton
        }

        fun start(millisInFuture: Long) {
            timer = BetterTimer(timeRemainingTextView, millisInFuture, 1000)
            timer?.onTimeExpired?.add(GoToMainActivity)
            timer?.start()
        }

        fun extendBy(seconds: Int) {
            timer?.cancel()
            val timeRemaining: Int = timeRemainingTextView.text.toString().toInt() // Nice
            val millisInFuture: Long = ((timeRemaining + seconds) * 1000).toLong()
            this.start(millisInFuture)
            extendTimerButton.isEnabled = true
        }
    }

    class BetterTimer(
        var timeRemainingTextView: TextView,
        millisInFuture: Long,
        countDownInterval: Long
    ) : CountDownTimer(
        millisInFuture, countDownInterval
    ) {

        var onTimeExpired: MutableList<Runnable> = LinkedList()

        override fun onTick(millisUntilFinished: Long) {
            timeRemainingTextView.text = (millisUntilFinished / 1000).toString()
        }

        override fun onFinish() {
            onTimeExpired.forEach { runnable ->
                runnable.run()
            }
        }
    }
}
