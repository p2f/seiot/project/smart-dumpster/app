package com.p2f.smartdumpster

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import java.util.*

object Token {
    var value: UUID? = null
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val button: Button = findViewById(R.id.request_token_button)
        button.setOnClickListener {
            if (!BluetoothThread.initialized) {
                BluetoothThread.initialize(applicationContext, this)
            }
            if (BluetoothThread.initialized) {
                requestToken(applicationContext)
            }
        }
    }

    private fun requestToken(context: Context) {
        val queue = Volley.newRequestQueue(context)
        val url = "https://p2f-iot.herokuapp.com/dumpsters/getToken"
        try {
            val stringRequest = StringRequest(Request.Method.GET, url,
                Response.Listener {
                    val reply = JSONObject(it)
                    val replyIsValid = validateTokenReply(reply)
                    if (replyIsValid) {
                        toastPrint("Token received.")
                        Token.value = UUID.fromString(reply.getString("token"))
                        /////////////////////////////////////
                        // Switch to select trash activity //
                        /////////////////////////////////////
                        val intent = Intent(this, SelectTrashActivity::class.java)
                        startActivity(intent)
                    } else if (reply.has("message") && reply.has("code")) {
                        val message = reply.getString("message")
                        val code = reply.getInt("code")
                        toastPrint("($code) $message")
                    }
                },
                Response.ErrorListener {
                    toastPrint(it.localizedMessage)
                }
            )
            queue.add(stringRequest)
            toastPrint("Token request sent.")
        } catch (exception:Exception) {
            toastPrint(exception.localizedMessage)
        }
    }

    private fun validateTokenReply(reply: JSONObject): Boolean {
        return reply.has("code") and reply.has("message") and reply.has("token")
    }

    private fun toastPrint(text: String) {
        Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Activity.RESULT_OK) {
            toastPrint("Bluetooth adapter enabled.")
        } else {
            toastPrint("You have to enable the bluetooth adapter.")
        }
    }
}