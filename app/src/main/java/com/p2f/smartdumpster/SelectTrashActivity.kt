package com.p2f.smartdumpster

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import org.json.JSONObject

val openRequestMessage = JSONObject()

enum class TrashType {
    GLASS, PAPER, PLASTIC
}

class SelectTrashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_trash)
        openRequestMessage.put("request", "open")
        GoToTimeActivity.initialize(this)
        ///////////
        // Glass //
        ///////////
        val glassButton: Button = findViewById(R.id.glass_button)
        glassButton.setOnClickListener {
            sendOpenRequestMessage(TrashType.GLASS)
        }
        ///////////
        // Paper //
        ///////////
        val paperButton: Button = findViewById(R.id.paper_button)
        paperButton.setOnClickListener {
            sendOpenRequestMessage(TrashType.PAPER)
        }
        /////////////
        // Plastic //
        /////////////
        val plasticButton: Button = findViewById(R.id.plastic_button)
        plasticButton.setOnClickListener {
            sendOpenRequestMessage(TrashType.PLASTIC)
        }
    }

    private fun sendOpenRequestMessage(trashType: TrashType) {
        openRequestMessage.put("trashtype", trashType.ordinal)
        BluetoothThread.expectEcho = true
        BluetoothThread.onOpenedEventReceived.add(GoToTimeActivity)
        BluetoothThread.sendMessage(openRequestMessage)
    }

    object GoToTimeActivity : Runnable {

        private lateinit var context: Context

        fun initialize(context: Context) {
            this.context = context
        }

        override fun run() {
            val intent = Intent(context, TimeActivity::class.java)
            context.startActivity(intent)
        }
    }
}
