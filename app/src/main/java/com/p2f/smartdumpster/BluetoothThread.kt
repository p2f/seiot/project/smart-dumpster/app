package com.p2f.smartdumpster

import android.annotation.SuppressLint
import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.widget.Toast
import com.github.ivbaranov.rxbluetooth.BluetoothConnection
import com.github.ivbaranov.rxbluetooth.RxBluetooth
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import java.util.*
import java.util.function.Consumer

object BluetoothThread : Thread() {

    private lateinit var connection: BluetoothConnection
    private const val deviceName = "group7IOT"
    private lateinit var context: Context
    private lateinit var message: String
    private val uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    var initialized: Boolean = false

    //////////////////////
    // Receiving buffer //
    //////////////////////
    private const val receivingBufferSize = 64
    private var receivedBytes = ByteArray(receivingBufferSize)
    private var receivedBytesCount = 0
    private const val terminator = '\n'

    ////////////
    // Events //
    ////////////
    var expectEcho = false
    var onClosingEventReceived: MutableList<Runnable> = LinkedList()
    var onEchoReceived: MutableList<Runnable> = LinkedList()
    var onOpenedEventReceived: MutableList<Runnable> = LinkedList()
    var onExtendTimeEventReceived: MutableList<Consumer<Int>> = LinkedList()
    var onExtendTimeDepletedEventReceived: MutableList<Consumer<Int>> = LinkedList()

    @SuppressLint("CheckResult")
    fun initialize(context: Context, activity: Activity) {
        this.context = context
        val rxBluetooth = RxBluetooth(context)
        /////////////////////////////////////////////////////////
        // Check if bluetooth adapter is available and enabled //
        /////////////////////////////////////////////////////////
        if (!rxBluetooth.isBluetoothAvailable) {
            toastPrint("This device doesn't support bluetooth.")
        } else if (!rxBluetooth.isBluetoothEnabled) {
            val requestEnableBluetooth = 1
            rxBluetooth.enableBluetooth(activity, requestEnableBluetooth)
        }
        ////////////////////////////////
        // Search dumpster controller //
        ////////////////////////////////
        val dumpsterController: BluetoothDevice? = findDumpsterController(rxBluetooth.bondedDevices)
        if (dumpsterController == null) {
            toastPrint("Dumpster controller not found.")
        } else {
            toastPrint("Dumpster controller found, connecting.")
            val socket = rxBluetooth.connectAsClient(dumpsterController, uuid).blockingGet()
            connection = BluetoothConnection(socket)
            toastPrint("Connected to dumpster controller.")
            ////////////////////////////////////////
            // Setup observer on receiving buffer //
            ////////////////////////////////////////
            val mainThread = AndroidSchedulers.mainThread()
            val ioScheduler = Schedulers.io()
            val responseObserver = connection
                .observeByteStream()
                .observeOn(mainThread)
                .subscribeOn(ioScheduler)
            responseObserver.subscribe(
                { receivedByte ->
                    receivedBytes[receivedBytesCount] = receivedByte
                    receivedBytesCount++
                    val lastCharacterReceived = receivedByte.toChar()
                    if (lastCharacterReceived == terminator) {
                        /////////////////////////////////
                        // Remove trailing empty bytes //
                        /////////////////////////////////
                        var receivedMessageString = String(receivedBytes, Charsets.UTF_8)
                        val terminatorIndex = receivedMessageString.indexOf(terminator) + 1
                        receivedMessageString = receivedMessageString.substring(0, terminatorIndex)
                        ////////////////////////////
                        // Do we expect and echo? //
                        ////////////////////////////
                        if (expectEcho) {
                            if (message == receivedMessageString) {
                                toastPrint("Echo received successfully: $receivedMessageString")
                                onEchoReceived.forEach { runnable ->
                                    runnable.run()
                                }
                                val jsonMessage = JSONObject(receivedMessageString)
                                if (jsonMessage.has("request") && jsonMessage.get("request") == "extend_time") {
                                    onExtendTimeEventReceived.forEach { consumer ->
                                        val seconds = jsonMessage.getInt("amount")
                                        consumer.accept(seconds)
                                    }
                                }
                            } else {
                                toastPrint("Expected echo, received different message.")
                                val jsonMessage = JSONObject(receivedMessageString)
                                if (jsonMessage.has("request") && jsonMessage.get("request") == "extend_time") {
                                    toastPrint("Extendable time depleted.")
                                    onExtendTimeDepletedEventReceived.forEach { consumer ->
                                        val seconds = jsonMessage.getInt("amount")
                                        consumer.accept(seconds)
                                    }
                                }
                            }
                            expectEcho = false
                        } else {
                            ////////////////////////////////////////////
                            // Handle messages sent by the controller //
                            ////////////////////////////////////////////
                            toastPrint("Received: $receivedMessageString")
                            val jsonMessage = JSONObject(receivedMessageString)
                            if (jsonMessage.has("event")) {
                                when (jsonMessage.get("event")) {
                                    "opened" -> {
                                        onOpenedEventReceived.forEach { runnable ->
                                            runnable.run()
                                        }
                                        this.message = receivedMessageString
                                        this.run()
                                    }
                                    "closing" -> {
                                        onClosingEventReceived.forEach { runnable ->
                                            runnable.run()
                                        }
                                    }
                                }
                            }
                        }
                        //////////////////////////////////////////////////
                        // Clean the receiving buffer after we are done //
                        //////////////////////////////////////////////////
                        for (index in 0..receivedBytesCount) {
                            receivedBytes[index] = 0
                        }
                        receivedBytesCount = 0
                    }
                }, { exception ->
                    toastPrint(exception.localizedMessage)
                }
            )
            initialized = true
        }
    }

    private fun toastPrint(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }

    private fun findDumpsterController(bondedDevices: Set<BluetoothDevice>?): BluetoothDevice? {
        bondedDevices?.forEach { device ->
            if (device.name == deviceName) {
                return device
            }
        }
        return null
    }

    override fun run() {
        connection.send(message)
        toastPrint("Message sent.")
    }

    fun sendMessage(message: JSONObject) {
        this.message = message.toString() + '\n'
        toastPrint("Sending " + this.message)
        this.run()
    }

    fun cancel() {
        try {
            connection.closeConnection()
        } catch (exception: Exception) {
            toastPrint("Could not close bluetooth connection.")
        }
    }
}